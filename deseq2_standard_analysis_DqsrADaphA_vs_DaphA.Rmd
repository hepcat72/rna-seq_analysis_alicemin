---
title: "RNA-Seq analysis using DESeq2"
author: "Lance Parsons <lparsons@princeton.edu>"
date: "August 24, 2015"
output: html_document
---

```{r child='init_session.Rmd'}
```

```{r child='load_data.Rmd'}
```

## Select samples
```{r}
# In this example, we'll use them all
dds <- ddsFull
# dds <- ddsFull[ , ddsFull$population == "POPULATION" ]
```

Sometimes it is necessary to drop levels of the factors, in case that all the samples for one or more
levels of a factor in the design have been removed. If time were included in the design formula, the
following code could be used to take care of dropped levels in this column.
```{r}
#dds$population <- droplevels( dds$population )
#dds$treatment <- droplevels( dds$treatment )
```


## Setup Experimental Design

### Setup design
We set our experimental `design` and select the `contrast` that we are interested in.
```{r setup_design}
# Experimental Design
design(dds) = ~ condition

# Contrast
contrast <- c("condition","DqsrADaphA","DaphA")

# Set base name for output files
output_basename <- sprintf("%s_vs_%s_standard_analysis", contrast[2], contrast[3])
```


```{r child='deseq2_standard_analysis_core.Rmd'}
```

```{r child='annotate_results_file.Rmd'}
```

## Display the version of R as well as any loaded packages.
```{r sessionInfo}
sessionInfo()
```
